#!/bin/bash

for i in $(ls tests/ci_cd_templates/); do
echo "$i"
aapg clean
aapg setup
aapg gen --config_file ./tests/ci_cd_templates/$i  --num_programs 16
make -C work all
cp work/bin/* ./files/.
cp work/log/*.dump ./files/.
cp work/objdump/*.objdump ./files/.
echo completing..
done